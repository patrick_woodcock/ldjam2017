var widthPercent = (window.innerWidth)/100;
var heightPercent = (window.innerHeight)/100;
var levelLength = 20; // number of screen widths

var game = new Phaser.Game(widthPercent*100, heightPercent*100, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update });

function preload () {

    game.load.image('player', 'assets/ship.png');
    game.load.image('star', 'assets/star2.png');
    game.load.image('baddie', 'assets/space-baddie.png');
    game.load.atlas('lazer', 'assets/laser.png', 'assets/laser.json');

}

var stars;
var baddies;
var lazers;
var player;
var cursors;
var fireButton;
var bulletTime = 0;
var frameTime = 0;
var frames;
var prevCamX = 0;

function create () {

    game.physics.startSystem(Phaser.Physics.P2JS);
    game.world.setBounds(0, 0, widthPercent * 400 * levelLength, heightPercent * 100);
    //  Turn on impact events for the world, without this we get no collision callbacks
    game.physics.p2.setImpactEvents(true);

    game.physics.p2.restitution = 0.8;

    //  Create our collision groups. One for the player, one for the pandas
    var playerCollisionGroup = game.physics.p2.createCollisionGroup();
    var baddieCollisionGroup = game.physics.p2.createCollisionGroup();

    //  This part is vital if you want the objects with their own collision groups to still collide with the world bounds
    //  (which we do) - what this does is adjust the bounds to use its own collision group.
    game.physics.p2.updateBoundsCollisionGroup();

    frames = Phaser.Animation.generateFrameNames('frame', 2, 30, '', 2);
    frames.unshift('frame02');

    stars = game.add.group();

    for (var i = 0; i < 128; i++)
    {
        stars.create(game.world.randomX, game.world.randomY, 'star');
    }

    baddies = game.add.group();

    for (var i = 0; i < 16; i++)
    {
        baddies.create(game.world.randomX, game.world.randomY, 'baddie');

    }

    baddies.forEachAlive(function(baddie) {
        // game.physics.enable(baddie,Phaser.Physics.ARCADE);
        game.physics.p2.enable(baddie);

        //  Tell the baddie to use the baddieCollisionGroup 
        baddie.body.setCollisionGroup(baddieCollisionGroup);
        //  Pandas will collide against themselves and the player
        //  If you don't set this they'll not collide with anything.
        //  The first parameter is either an array or a single collision group.
        baddie.body.collides([baddieCollisionGroup, playerCollisionGroup]);
    });

    lazers = game.add.group();

    player = game.add.sprite(widthPercent*20, heightPercent*5, 'player');
    player.anchor.x = 0.5;

    game.physics.p2.enable(player, false);
    //  Set the players collision group
    player.body.setCollisionGroup(playerCollisionGroup);
    
    player.body.collides(baddieCollisionGroup, hitBaddie, this);
    player.body.fixedRotation = true;

    // game.camera.follow(player, Phaser.Camera.FOLLOW_LOCKON, 0.1);

    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    prevCamX = game.camera.x;
    var spriteGroup = game.add.group();
    spriteGroup.add(baddies);
    spriteGroup.add(lazers);
    spriteGroup.add(player);
    spriteGroup.add(stars);
    spriteGroup.scale.set((widthPercent*100)/800);

}

function update () {

    game.camera.x +=10;

    if (cursors.left.isDown)
    {
        player.body.moveLeft(200);
        // player.x -= 8;
        player.scale.x = -1;
    }
    else if (cursors.right.isDown)
    {
        player.body.moveRight(200);
        //player.x += 8;
        player.scale.x = 1;
    }

    if (cursors.up.isDown)
    {
        player.body.moveUp(200);
        // player.y -= 8;
    }
    else if (cursors.down.isDown)
    {
        player.body.moveDown(200);
        // player.y += 8;
    }

    if (fireButton.isDown)
    {
        fireBullet();
    }

    lazers.forEachAlive(updateBullets, this);

    prevCamX = game.camera.x;

    baddies.forEachAlive(function(baddie) {
        accelerateToObject(baddie,player,200);
    });

}

function updateBullets (lazer) {

    // if (game.time.now > frameTime)
    // {
    //     frameTime = game.time.now + 500;
    // }
    // else
    // {
    //     return;
    // }

    //  Adjust for camera scrolling
    var camDelta = game.camera.x - prevCamX;
    lazer.x += camDelta;

    if (lazer.animations.frameName !== 'frame30')
    {
        lazer.animations.next();
    }
    else
    {
        if (lazer.scale.x === 1)
        {
            lazer.x += 16;

            if (lazer.x > (game.camera.view.right - 224))
            {
                lazer.kill();
            }
        }
        else
        {
            lazer.x -= 16;

            if (lazer.x < (game.camera.view.left - 224))
            {
                lazer.kill();
            }
        }
    }

}

function fireBullet () {

    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        lazer = lazers.getFirstDead(true, player.x + 24 * player.scale.x, player.y + 8, 'lazer');

        lazer.animations.add('fire', frames, 60);
        lazer.animations.frameName = 'frame02';

        lazer.scale.x = player.scale.x;

        if (lazer.scale.x === 1)
        {
            // lazer.anchor.x = 1;
        }
        else
        {
            // lazer.anchor.x = 0;
        }

        //  Lazers start out with a width of 96 and expand over time
        // lazer.crop(new Phaser.Rectangle(244-96, 0, 96, 2), true);

        bulletTime = game.time.now + 250;
    }

}

function accelerateToObject(obj1, obj2, speed) {
    if (typeof speed === 'undefined') { speed = 60; }
    var angle = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
    obj1.body.rotation = angle + game.math.degToRad(90);  // correct angle of angry bullets (depends on the sprite used)
    obj1.body.force.x = Math.cos(angle) * speed;    // accelerateToObject 
    obj1.body.force.y = Math.sin(angle) * speed;
}


function hitBaddie(body1, body2) {
    // debugger;    
        //  body1 is the space ship (as it's the body that owns the callback)
        //  body2 is the body it impacted with, in this case our panda
        //  As body2 is a Phaser.Physics.P2.Body object, you access its own (the sprite) via the sprite property:
        body2.sprite.alpha -= 0.1;
    
    }